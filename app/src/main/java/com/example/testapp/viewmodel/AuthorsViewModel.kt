package com.example.testapp.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.testapp.data.Author
import com.example.testapp.data.AuthorRepository


class AuthorsViewModel:ViewModel() {
    var firstName = ObservableField("")
    var lastName = ObservableField("")

    fun searchAuthors() {
        AuthorRepository.getAuthors(firstName.get(), lastName.get())
    }

    fun getAuthors(): LiveData<List<Author>> {
        return AuthorRepository.getAuthors()
    }
}