package com.example.testapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.testapp.data.Author
import com.example.testapp.data.AuthorRepository

class ConcreteAuthorViewModel: ViewModel() {
    fun getAuthorInformation(authorId: Int): LiveData<Author> {
        return AuthorRepository.getAuthorInformation(authorId)
    }
}