package com.example.testapp.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.testapp.R
import com.example.testapp.data.Author
import com.example.testapp.databinding.HolderAuthorBinding

class SearchAuthorsAdapter: RecyclerView.Adapter<SearchAuthorsAdapter.AuthorViewHolder>() {
    private var authorsList: List<Author> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AuthorViewHolder {
        val binding = HolderAuthorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AuthorViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AuthorViewHolder, position: Int) {
        holder.bind(authorsList[position])
    }

    override fun getItemCount(): Int = authorsList.size

    fun update(newList: List<Author>) {
        authorsList = newList
        notifyDataSetChanged()
    }
//
    class AuthorViewHolder(private val binding: HolderAuthorBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(author: Author) {
            binding.author = author
            itemView.setOnClickListener {
                it.findNavController().navigate(R.id.action_searchAuthorFragment_to_authorFragment,
                    bundleOf("authorId" to author.authorId))
            }
        }
}
}