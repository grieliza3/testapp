package com.example.testapp.view.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.testapp.databinding.FragmentSearchAuthorBinding
import com.example.testapp.view.adapters.SearchAuthorsAdapter
import com.example.testapp.viewmodel.AuthorsViewModel

class SearchAuthorsFragment : Fragment() {
    private val FIRSTNAME_KEY = "firstName"
    private val LASTNAME_KEY = "lastName"

    val viewModel: AuthorsViewModel by lazy {
        ViewModelProvider(this@SearchAuthorsFragment).get(AuthorsViewModel::class.java)
    }
    lateinit var binding: FragmentSearchAuthorBinding
    var adapter: SearchAuthorsAdapter = SearchAuthorsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getPreferences()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchAuthorBinding.inflate(layoutInflater, container, false)
        binding.viewmodel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.foundAuthors.adapter = adapter
        configureLiveData()
    }

    private fun configureLiveData() {
        viewModel.getAuthors().observe(viewLifecycleOwner, {
            adapter.update(it)
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(FIRSTNAME_KEY, viewModel.firstName.get())
            putString(LASTNAME_KEY, viewModel.lastName.get())
            apply()
        }
    }

    private fun getPreferences() {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = ""
        viewModel.firstName.set(sharedPref.getString(FIRSTNAME_KEY, defaultValue))
        viewModel.lastName.set(sharedPref.getString(LASTNAME_KEY, defaultValue))
    }

}