package com.example.testapp.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.testapp.R
import com.example.testapp.databinding.FragmentAuthorBinding
import com.example.testapp.viewmodel.ConcreteAuthorViewModel
import kotlin.properties.Delegates

class AuthorFragment : Fragment() {
    private val ARG_PARAM1 = "authorId"
    private var authorId by Delegates.notNull<Int>()

    private lateinit var binding: FragmentAuthorBinding
    val viewModel: ConcreteAuthorViewModel by lazy {
        ViewModelProvider(this@AuthorFragment).get(ConcreteAuthorViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            authorId = it.getInt(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAuthorBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setupWithNavController(findNavController())
        binding.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        configureLiveData()
    }

    private fun configureLiveData() {
        viewModel.getAuthorInformation(authorId).observe(
            viewLifecycleOwner, {
                binding.toolbar.title = it.name
                binding.author = it
            }
        )
    }
}