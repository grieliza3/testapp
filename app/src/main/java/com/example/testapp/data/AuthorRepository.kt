package com.example.testapp.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testapp.api.NetworkService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object AuthorRepository {
    private val authorList: MutableLiveData<List<Author>> = MutableLiveData()

    fun getAuthors(): LiveData<List<Author>> {
        return authorList
    }

    fun addAuthors(newAuthors: List<Author>) {
        authorList.postValue(authorList.value?.plus(newAuthors))
    }

    fun getAuthors(firstName: String?, lastName: String?) {
        GlobalScope.launch(Dispatchers.IO) {
            NetworkService.searchAuthors(firstName, lastName)
        }
    }

    fun removeAuthors() {
        authorList.postValue(listOf())
    }

    fun getAuthorInformation(authorId: Int): LiveData<Author> {
        val result = MutableLiveData<Author>()
        GlobalScope.launch {
            result.postValue(NetworkService.getAuthorInformation(authorId))
        }
        return result
    }
}