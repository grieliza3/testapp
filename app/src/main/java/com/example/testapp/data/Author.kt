package com.example.testapp.data

import com.google.gson.annotations.SerializedName

data class Author(@SerializedName("authorid") val authorId: Int,
                  @SerializedName("authordisplay") var name: String,
                  var spotlight: String?, var photocredit: String?)