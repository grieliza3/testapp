package com.example.testapp.api

import com.example.testapp.data.Author

data class SearchResponse(val author: List<Author>)