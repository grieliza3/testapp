package com.example.testapp.api

import com.example.testapp.data.Author
import com.example.testapp.data.AuthorRepository
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.await
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory

object NetworkService {
    private var retrofit: Retrofit? = null
    private const val url = "https://reststop.randomhouse.com/"
    private val apiService: AuthorsService = getRetrofit().create(AuthorsService::class.java)


    private fun getRetrofit(): Retrofit {
        if (retrofit == null) {
            val gson = GsonBuilder()
                    .setLenient()
                    .create()

            retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        }
        return retrofit!!
    }

    suspend fun searchAuthors(firstName: String?, lastName: String?) {
        AuthorRepository.removeAuthors()
        var start = 0
        val max = 40
        var response = apiService.loadAuthors(start,max, firstName, lastName).awaitResponse()
        response.body()?.let { searchResponse ->
            while (!response.body()?.author.isNullOrEmpty()) {
                AuthorRepository.addAuthors(searchResponse.author)
                start += max
                response = apiService.loadAuthors(start, max, firstName, lastName).awaitResponse()
            }
        }
    }

    suspend fun getAuthorInformation(authorId: Int): Author {
        val response = apiService.getAuthorInformation(authorId).await()
        return response
    }
}