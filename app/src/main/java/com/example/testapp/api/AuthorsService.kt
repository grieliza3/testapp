package com.example.testapp.api

import com.example.testapp.data.Author
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface AuthorsService {
    @Headers("Accept: application/json")
    @GET("resources/authors")
    fun loadAuthors(
        @Query("start") start: Int,
        @Query("max") max: Int,
        @Query("firstName") firstName: String?,
        @Query("lastName") lastName: String?,
        @Query("expandLevel") expandLevel: Int = 1
    ): Call<SearchResponse>

    @Headers("Accept: application/json")
    @GET("resources/authors/{authorId}")
    fun getAuthorInformation(@Path("authorId") authorId: Int): Call<Author>
}